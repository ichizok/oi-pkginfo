#!/usr/bin/env python

import os, sys

def linkdirs(ver):
    import glob

    jdks = sorted(glob.glob('/usr/jdk/jdk{0}*'.format(ver)))
    if len(jdks) == 0:
        print('No target jdk ({0}).'.format(ver))
        sys.exit(0)

    JAVADIR_LINK = '/usr/java'
    LASTEST_LINK = '/usr/jdk/latest'

    os.unlink(JAVADIR_LINK)
    os.unlink(LASTEST_LINK)

    jdk = os.path.basename(jdks[-1])
    os.symlink(os.path.join('jdk', jdk), JAVADIR_LINK)
    os.symlink(jdk, LASTEST_LINK)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Setup JDK environment')
    parser.add_argument('version', help='select JDK version (e.g. 1.7)')
    args = parser.parse_args()

    if os.geteuid() != 0:
        print('Do with privilege.')
        sys.exit(1)

    linkdirs(args.version)
