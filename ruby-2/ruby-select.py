#!/usr/bin/env python

from __future__ import print_function
import os, sys

def link_ruby(ver):
    import glob

    PREFIX = '/usr/local'
    RUBY_DIR = os.path.join(PREFIX, 'ruby', ver)
    if not os.path.isdir(RUBY_DIR):
        print('ruby {0} is not installed.'.format(ver), file=sys.stderr)
        sys.exit(1)

    BINDIR = os.path.join(PREFIX, 'bin', 'amd64')
    LIBDIR = os.path.join(PREFIX, 'lib', 'amd64')
    RUBY_BINDIR = os.path.join(RUBY_DIR, 'bin', 'amd64')
    RUBY_LIBDIR = os.path.join(RUBY_DIR, 'lib', 'amd64')
    RUBY_SUFFIX = ver.replace('.', '')

    def make_symlink(src, dest):
        print('link {0} -> {1}'.format(dest, src))
        if os.path.lexists(dest):
            os.unlink(dest)
        os.symlink(src, dest)

    for rbpath in glob.iglob(os.path.join(RUBY_BINDIR, '*' + RUBY_SUFFIX)):
        rbname = os.path.basename(rbpath)

        if os.path.exists(os.path.join(BINDIR, rbname)):
            # BINDIR/ruby -> BINDIR/rubyXX
            make_symlink(rbname, os.path.join(BINDIR, rbname[: -len(RUBY_SUFFIX)]))

    for rbpath in glob.iglob(os.path.join(RUBY_LIBDIR, 'libruby*')):
        rbname = os.path.basename(rbpath)

        # LIBDIR/libruby* -> RUBY_LIBDIR/libruby*
        make_symlink(os.path.relpath(LIBDIR, rbpath), os.path.join(LIBDIR, rbname))

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Setup ruby environment')
    parser.add_argument('version', help='select ruby version (e.g. 2.0)')
    args = parser.parse_args()

    if os.geteuid() != 0:
        print('Do with privilege.')
        sys.exit(1)

    link_ruby(args.version)
