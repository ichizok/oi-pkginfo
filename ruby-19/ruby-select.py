#!/usr/bin/env python

from __future__ import print_function
import os, sys

def link_ruby(ver):
    import glob

    PREFIX = '/usr/local'
    RUBY_DIR = os.path.join(PREFIX, 'ruby', ver)
    if not os.path.isdir(RUBY_DIR):
        print('ruby {0} is not installed.'.format(ver), file=sys.stderr)
        sys.exit(1)

    BINDIR = os.path.join(PREFIX, 'bin', 'amd64')
    RUBY_BINDIR = os.path.join(RUBY_DIR, 'bin', 'amd64')
    RUBY_SUFFIX = ver.replace('.', '')

    def link_rubybin(src, dest):
        print('link {0} -> {1}'.format(dest, src))
        if os.path.exists(dest):
            os.unlink(dest)
        os.symlink(src, dest)

    for rbpath in glob.iglob(os.path.join(RUBY_BINDIR, '*' + RUBY_SUFFIX)):
        rbname = os.path.basename(rbpath)

        if os.path.exists(os.path.join(BINDIR, rbname)):
            # BINDIR/ruby -> BINDIR/rubyXX
            link_rubybin(rbname, os.path.join(BINDIR, rbname[: -len(RUBY_SUFFIX)]))

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Setup ruby environment')
    parser.add_argument('version', help='select ruby version (e.g. 1.9)')
    args = parser.parse_args()

    if os.geteuid() != 0:
        print('Do with privilege.')
        sys.exit(1)

    link_ruby(args.version)
