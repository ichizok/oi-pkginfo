#!/sbin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)redis	1.1	09/02/18 SMI"

. /lib/svc/share/smf_include.sh

# SMF_FMRI is the name of the target service. This allows multiple instances 
# to use the same script.

getproparg() {
	val=`svcprop -p $1 $SMF_FMRI`
	[ -n "$val" ] && echo $val
}

REDISBASE=`dirname $(getproparg redis/bin)`
REDISBIN=`getproparg redis/bin`
REDISDATA=`getproparg redis/data`
REDISCONF=/etc/redis.conf
PIDFILE=`ggrep -s pidfile ${REDISCONF} | cut -d' ' -f2`


if [ -z $SMF_FMRI ]; then
	echo "SMF framework variables are not initialized."
	exit $SMF_EXIT_ERR
fi

if [ ! -d ${REDISDATA} ]; then
	echo "redis/data directory ${REDISDATA} is not a valid redis data directory"
	exit $SMF_EXIT_ERR_CONFIG
fi

if [ ! -f ${REDISCONF} ]; then
	REDISCONF=
fi


redis_start() 	{
	REDISVAL=`getproparg redis/enable_64bit nosql:redis`
	if [ "$REDISVAL" != "" ] ; then
	case "$REDISVAL" in
	true|1)
		PLATFORM=`isainfo -b`
		if [ "${PLATFORM}" != "64" ]; then
			echo "This system is not capable of supporting 64-bit applications."
			echo "Set the \"enable_64bit\" property value to \"false\" to start the 32-bit server."
			exit $SMF_EXIT_ERR_CONFIG
		else
			echo ${REDISBIN}/64/redis-server ${REDISCONF}
			${REDISBIN}/64/redis-server ${REDISCONF}
		fi
	;;
	false|0)
		echo ${REDISBIN}/redis-server ${REDISCONF}
		${REDISBIN}/redis-server ${REDISCONF}
	;;
	esac
	fi
}


redis_stop()	{
	if [ -f ${PIDFILE} ]; then
	pkill -INT redis-server
	fi
}

case "$1" in
'start')
	redis_start 
	;;

'stop')
	redis_stop
	;;


*)
	echo "Usage: $0 {start|stop}"
	exit 1
	;;

esac
exit $SMF_EXIT_OK
